
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html lang="en">
  <head>
    <title>cloud-project-ecip</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
  </head>
  <body>
    <div class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <div class="navbar-brand">CloudBooks</div>
        </div>
        <ul class="nav navbar-nav">
          <li><a href="/">Books</a></li>
        </ul>
        <p class="navbar-text navbar-right">
          <c:choose>
          <c:when test="${not empty userEmail}">
          <!-- using pageContext requires jsp-api artifact in pom.xml -->
          <a href="/logout">
            <c:if test="${not empty userImageUrl}">
              <img class="img-circle" src="${fn:escapeXml(userImageUrl)}" width="24">
            </c:if>
            Logged as: ${fn:escapeXml(userEmail)}
          </a>
          </c:when>
          <c:otherwise>

          <a href="/login" class="btn btn-primary">Login</a>
          </c:otherwise>
          </c:choose>
        </p>
      </div>
    </div>
    <c:import url="/${page}.jsp" />
  </body>
</html>
<!-- [END base]-->
